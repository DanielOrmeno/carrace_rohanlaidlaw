﻿using System;

namespace CarRace
{
    public sealed class SynchronizedRandomGenerator
    {
        public SynchronizedRandomGenerator(int minValue, int maxValue) 
        {
            MinValue = minValue;
            MaxValue = maxValue;
            RandomGenerator = new Random();
        }

        private int MinValue { get; }
        private int MaxValue { get; }
        private Random RandomGenerator { get; }

        public int Next() 
        {
            return RandomGenerator.Next(MinValue, MaxValue);
        }
    }
}
