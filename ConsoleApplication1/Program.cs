﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace CarRace
{
    public static class Program
    {
        public static void Main()
        { 
            var carsInRace = RaceSpecifications();
            var cars = new List<Car>();
            var carThreads = new Thread[carsInRace];

            RacePreparations(carsInRace, cars, carThreads);

            Console.WriteLine("Press Enter to Start the Race.");
            Console.ReadLine();

            RaceStart(carThreads);

            Console.ReadLine();
        }

        /// <summary>
        /// Allows the user to enter desired number of cars that will participate in the race.
        /// </summary>
        /// <returns>The user's text parsed as an integer.</returns>
        private static int RaceSpecifications()
        {
            while (true){
                Console.Write("How many cars will participate in the race?");
                var input = Console.ReadLine();

                int userTextParsed;
                if ((!int.TryParse(input, out userTextParsed)) || (userTextParsed <= 0))
                    continue;

                Console.WriteLine(userTextParsed + " cars will participate in the race.");
                return userTextParsed;
            }
        }

        /// <summary>
        /// Creates appropriate number of cars and dedicated threads to operate on. 
        /// </summary>
        /// <param name="carsInRace">The number of cars the user has chosen to participate in the race.</param>
        /// <param name="cars">An array with which to store the newly created Car objects.</param>
        /// <param name="carThreads">An array with which to store the newly created threads for each of the Car objects.</param>
        private static void RacePreparations(int carsInRace, List<Car> cars, Thread[] carThreads)
        {
            if (cars == null) throw new ArgumentNullException(nameof(cars));

            const int raceDistance = 1000;
            const int minValue = 0;
            const int maxValue = 100;

            for (var i = 0; i < carsInRace; i++)
            {
                cars.Add(new Car(i, new SynchronizedRandomGenerator(minValue, maxValue), raceDistance));
                var index = i;
                carThreads[i] = new Thread(() => cars[index].Race());
                Console.WriteLine("Car " + i + " ready.");

                //Allow some time to pass for a unique RNG seed
                Thread.Sleep(20);
            }

            Console.WriteLine("Race Preparations made...");
        }

        /// <summary>
        /// Cycles through each of the threads hosting the Car objects and starts them.
        /// </summary>
        /// <param name="carThreads">An array that stores each of the threads for the Car objects.</param>
        private static void RaceStart(Thread[] carThreads)
        {
            carThreads.ToList().ForEach(x => x.Start());
        }
    }
}
