﻿using System;

namespace CarRace
{
    public sealed class Car
    {
        public Car(int carId, SynchronizedRandomGenerator randomGenerator, int destKm){
            CarId = carId;
            DestKm = destKm;
            RandomGenerator = randomGenerator;
        }

        private int CarId { get; set; }
        private int DestKm { get; set; }
        private SynchronizedRandomGenerator RandomGenerator { get; }

        public void Race() {
            var totalKm = 0;

            //Each "round" the total travel of the car is checked against the length of the race track.
            //Travel is written to the console. 
            while (totalKm < DestKm)
            {
                var carTravel = RandomGenerator.Next();
                totalKm = totalKm + carTravel;
                Console.WriteLine("Car " + CarId + " has advanced " + totalKm + " km");
                //Thread.Sleep(100); Why do we pause the thread?
            }

            Console.WriteLine("CAR " + CarId + " HAS CROSSED THE FINISH LINE");
        }
    }
}
